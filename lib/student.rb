## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.


class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    @first_name + ' ' + @last_name
  end

  def courses
    @courses
  end

  def enroll(new_course)
    return 'error' if @courses.include?(new_course)
    raise Exception.new('conflict') if self.courses.any? {|i| i.conflicts_with?(new_course)}
    @courses << new_course
    @courses.last.students << self
  end

  def course_load
    hash = Hash.new(0)
    @courses.each do |i|
      hash[i.department] += i.credits
    end
    hash
  end

end
